alert('Метод forEach перебирает каждый елемент массива и выполняет для этих' +
    'елементов заданную функцию')
function filteredBy (arr,typeOfData) {

    const newArray = arr.filter(item => {
        return (typeof item !== typeOfData);
    });
    return newArray; // короткая запись

    // return {      // длинная запись
    //     array: newArray
    // }
}
filteredBy([],);
console.log(filteredBy([12, 21, 21,'hello', 'world', undefined, 323, 'how','are', 23, null, 11, 'you', 'everything', 'ok', undefined],'string'));







